
import java.util.ArrayList;
import java.util.List;

public class Node_tree {
	
	/*
	 * Author : Natthakit Srikanjanapert
	 * Sample Code for CS MSU bachalor's degree
	 * */
	
	private String node_name 		= "empty";
	private String br_name 			= "empty";
	private int idx_min_val			= -1;
	private int idx_max_val			= -1;
	private List<Integer> my_seleted 	= new ArrayList<Integer>();
	
	private boolean im_leaf 		= false;
	private String leaf_val 		= "empty";
	
	private int data[][];
	private int distribution[][];
	private int distinct[];
	private int distinct_table[][];
	
	Node_tree parent;
	Node_tree children[];
	
	public Node_tree() { }
	
	public Node_tree(String name) {
		this.node_name = name;
	}
	
	/*
	 * General Method
	 * */
	
	public void setChildren() {
		if(this.distribution == null){
			System.out.println("Error to set Child Node !");
			return;
		}else{
			this.children = new Node_tree[distribution.length];
		}
	}
	
	public void setChildren(String size) {
		this.children = new Node_tree[Integer.parseInt(size)];
	}
	
	/*
	 * Getter and Setter Method
	 * */
	
	public String getNode_name() {
		return node_name;
	}
	public void setNode_name(String node_name) {
		this.node_name = node_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public boolean isIm_leaf() {
		return im_leaf;
	}
	public void setIm_leaf(boolean im_leaf) {
		this.im_leaf = im_leaf;
	}
	public String getLeaf_val() {
		return leaf_val;
	}
	public void setLeaf_val(String leaf_val) {
		this.leaf_val = leaf_val;
	}
	public Node_tree getParent() {
		return parent;
	}
	public void setParent(Node_tree parent) {
		this.parent = parent;
	}
	public Node_tree[] getChildren() {
		return children;
	}
	public void setChildren(Node_tree[] children) {
		this.children = children;
	}
	public List<Integer> getMy_seleted() {
		return my_seleted;
	}
	public void setMy_seleted(List<Integer> my_seleted) {
		this.my_seleted = my_seleted;
	}
	public int getIdx_min_val() {
		return idx_min_val;
	}
	public void setIdx_min_val(int idx_min_val) {
		this.idx_min_val = idx_min_val;
	}
	public int[][] getData() {
		return data;
	}
	public void setData(int[][] data) {
		this.data = data;
	}
	public int[][] getDistribution() {
		return distribution;
	}
	public void setDistribution(int[][] distribution) {
		this.distribution = distribution;
	}

	public int getIdx_max_val() {
		return idx_max_val;
	}

	public void setIdx_max_val(int idx_max_val) {
		this.idx_max_val = idx_max_val;
	}

	public int[] getDistinct() {
		return distinct;
	}

	public void setDistinct(int[] distinct) {
		this.distinct = distinct;
	}

	public int[][] getDistinct_table() {
		return distinct_table;
	}

	public void setDistinct_table(int[][] distinct_table) {
		this.distinct_table = distinct_table;
	}
	
}