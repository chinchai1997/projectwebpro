
public class Main_lab2 {

	public static void main(String[] args) {
		ID3_class id3 = new ID3_class("weather_nominal2.txt");
		InfoGain temp[] = id3.doGain();
		id3.maxGain(temp);
		System.out.println("Maximum of Gain : "+temp[id3.getMaxIdx()].getGain_name());
		
		id3.result_tree = new Node_tree(temp[id3.getMaxIdx()].getGain_name());

		id3.result_tree.setData(temp[id3.getMaxIdx()].getData());
		id3.result_tree.setDistinct_table(temp[id3.getMaxIdx()].distinct_table);
		id3.result_tree.setIdx_max_val(id3.getMaxIdx());
		id3.result_tree.setDistinct(temp[id3.getMaxIdx()].count_distinct);
		id3.result_tree.setChildren(temp[id3.getMaxIdx()].child_size[id3.getMaxIdx()]);
		
		id3.expand(id3.result_tree,id3,id3.getMaxIdx()); // do recursive
		System.out.println();
	}
	
}
