<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home','Usercontroller@home');
Route::get('/about','Usercontroller@about');
Route::get('/roomsrates','Usercontroller@roomsrates');
Route::get('/location','Usercontroller@location');
Route::get('/promotion','Usercontroller@promotion');
Route::get('/book','Usercontroller@book');
Route::post('/login','Usercontroller@login');
Route::get('/register','Usercontroller@register');
Route::post('/insert_user','Usercontroller@insert_user');
Route::get('/backend/admin','Usercontroller@admin');
Route::get('/logout','Usercontroller@logout');
Route::get('/status','Usercontroller@status');
Route::get('/detailstatus/{id}','Usercontroller@detailstatus');
Route::get('/question','Usercontroller@question');
Route::get('/addquestion','Usercontroller@addquestion');
Route::post('/addquestion1','Usercontroller@addquestion1');

Route::get('/admin','Usercontroller@admin');
//*************************************************
Route::get('/employee','Usercontroller@employee');
Route::get('/employee1/{id}', 'Usercontroller@employee1') ;
Route::get('/employee2/{id}', 'Usercontroller@employee2') ;
Route::post('/employee3', 'Usercontroller@employee3') ;
Route::get('/de1employee/{id}', 'Usercontroller@de1employee') ;
Route::get('/de2employee/{id}', 'Usercontroller@de2employee') ;
//*************************************************
Route::get('/boss','Usercontroller@boss');
Route::get('/de1boss/{id}', 'Usercontroller@de1boss') ;
Route::get('/de2boss/{id}', 'Usercontroller@de2boss') ;
Route::get('/de3boss/{id}', 'Usercontroller@de3boss') ;

//*************************************************

Route::get('/addroom','Usercontroller@addroom');
Route::post('/adddataroom','Usercontroller@adddataroom');
Route::get('/editroom/{id}','Usercontroller@editroom');
Route::post('/updateroom','Usercontroller@updateroom');
Route::get('/deleteroom/{id}','Usercontroller@deleteroom');

Route::get('/addemployee','Usercontroller@addemployee');
Route::post('/adddataemployee','Usercontroller@adddataemployee');
Route::get('/editeemployee/{id}','Usercontroller@editeemployee');
Route::post('/updateemployee','Usercontroller@updateemployee');
Route::get('/deleteemployee/{id}','Usercontroller@deleteemployee');

Route::get('/addpromotion','Usercontroller@addpromotion');
Route::post('/adddatapromotion','Usercontroller@adddatapromotion');
Route::get('/editepromotion/{id}','Usercontroller@editepromotion');
Route::post('/updatepromotion','Usercontroller@updatepromotion');
Route::get('/deletepromotion/{id}','Usercontroller@deletepromotion');

Route::post('/booking','Usercontroller@booking');
// Route::get('/adddataroom',function (){
// 	return "kfodpsfkpodsf";
// });

// Route::get('/employee','Usercontroller@employee');



Route::get('/', function () {
    return view('welcome');
});

Route::get('/check-connect', function () {
    if(DB::connection()->getDatabaseName()){
    	return "Yes! successfully connected to the DB:".DB::connection()->getDatabaseName();
    }else{
    	return 'Connection Fslse !!';
    }
});

