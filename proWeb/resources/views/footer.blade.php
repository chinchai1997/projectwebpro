<footer id="footer" class="color color-primary mt-0 pt-5 pb-5">
	<div class="container">
		<div class="row pt-3 pb-3">
			<div class="col-lg-3 offset-lg-4 mb-0">
				<!-- <img alt="Porto" class="img-fluid logo" style="max-width: 145px;" src="{{asset('img/demos/hotel/logo-hotel-footer.png')}}"> -->
			</div>
			<div class="col-lg-3 mb-0">
				<div class="footer-info">
					<i class="icon-location-pin icons"></i>
					<label>address</label>
					<strong>Nai Mueang Subdistrict, Mueang Nakhon Ratchasima District, Nakhon Ratchasima</strong>
				</div>
			</div>
			<div class="col-lg-2 mb-0">
				<div class="footer-info">
					<i class="icon-phone icons"></i>
					<label>call us</label>
					<strong>(800) 1234-5678</strong>
				</div>
			</div>
		</div>
	</div>
</footer>