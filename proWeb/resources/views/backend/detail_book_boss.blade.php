<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col-lg-3">
							<div class="tabs tabs-vertical tabs-left tabs-navigation">
								<ul class="nav nav-tabs ">
									<li class="nav-item active">
										<a class="nav-link" href="/boss"> ย้อนกลับ</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
								<h3 class="mb-0 pb-0 text-uppercase">รายละเอียดการจองห้อง</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>
								<div class="row">
									<div class="col-6 border">
										<div class="nav-link active">
											<br>
											<label>ชื่อผู้จอง :  </label><br>
											<label class="border ml-4"><?=$information['name']?> </label>
											<br>
											<label>ชื่อห้องพัก :</label><br>
											<label class="border ml-4"><?=$information['name_room']?></label>
											<br>
											<label>ประเภทห้องพัก :</label><br>
											<label class="border ml-4"><?=$information['type_room']?> </label>
											<br>
											<label>ราคาห้องพัก :</label><br>
											<label class="border ml-4"><?=$information['price']?> บาท </label>
											<br>
										</div>
									</div>

									<div class="col-6 border">
										<div class="nav-link active">
											<br>
											<label>จำนวนผู้เข้าพัก (เด็ก) :  </label><br>
											<label class="border ml-4"><?=$information['child']?> คน</label>
											<br>
											<label>จำนวนผู้เข้าพัก (ผู้ใหญ่) :  </label><br>
											<label class="border ml-4"><?=$information['adult']?> คน</label>
											<br>
											<label>วันที่เข้าพัก :</label><br>
											<label class="border ml-4"><?=$information['start']?></label>
											<br>
											<label>วันที่ออก :</label><br>
											<label class="border ml-4"><?=$information['stop']?> </label>
											<br>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</section>
		<?=view('footer');?>
	</div>

</div>
<?=view('js');?>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function() {
		$('#about').addClass('active');
	});
</script>
