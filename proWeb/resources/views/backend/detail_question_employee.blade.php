<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">
						<div class="col-lg-3">
						</div>
						<div class="col-lg-6">
							<div class="tab-pane tab-pane-navigation active" id="">
								<h3 class="mb-0 pb-0 text-uppercase">ตอบกลับลูกค้า</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>
							</div>
							<form action="/employee3" method="post" >
								{{ csrf_field() }}
								<input type="hidden" value='<?=$data['id']?>' name="id"  >
								<label> ชื่อลูกค้า : <?=$data['name']?></label>
								<br>
								<label> คำถาม : <?=$data['ques']?></label><br>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">ตอบกลับ</span>
									</div>
									<textarea class="form-control" aria-label="answer" name="text"></textarea>
								</div>
								<!-- <br> -->
								<div class="row">
									<div class="col-3">
										<a class="" href="/employee">ย้อนกลับ</a>
									</div>
									<div class="col-6">

									</div>
									<div class="col-3 ">
										<button type="submit" class="btn btn-light text-right mr-0 ml-4">ตอบกลับ</button> 
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
				<?=view('footer');?>
			</div>
		</div>
		<?=view('js');?>
	</body>
	</html>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#about').addClass('active');
		});
	</script>
