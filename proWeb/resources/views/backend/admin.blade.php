<!DOCTYPE html>
<html class="boxed">
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Hotel | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<?=view('css');?>

</head>
<body>

	<?=view('header');?>

	<div class="body">
		<div role="main" class="main">
			<section class="section section-no-background section-no-border m-0">
				<div class="container">
					<div class="row mb-4">

						<div class="col-lg-3">
							<div class="tabs tabs-vertical tabs-left tabs-navigation">
								<ul class="nav nav-tabs col-sm-3">
									<li class="nav-item">
										<a class="nav-link" href="/home"> หน้าแรก</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation1" data-toggle="tab"> จัดการห้องพัก</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation3" data-toggle="tab"> จัดการพนักงาน</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#tabsNavigation4" data-toggle="tab"> จัดการโปรโมชั่น</a>
									</li>
									<li class="nav-item">
										<a class="nav-link active" href="/logout" > ออกจากระบบ</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
								<h3 class="mb-0 pb-0 text-uppercase">จัดการห้องพัก</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>ประเภทห้อง</th>
													<th>ชื่อห้อง</th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$rooms = DB::table('room')->get();
												foreach ($rooms as $room) {
													?>
													<tr>
														<td><?=$room->type?></td>
														<td><?=$room->name?></td>
														<td><a href="editroom/<?=$room->id?>" title="">แก้ไข</a></td>
														<td><a href="deleteroom/<?=$room->id?>" title="">ลบ</a></td>
													</tr>
													<?php 
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<a href="/addroom" title="">เพิ่มข้อมูลห้องพัก</a>
									</div>
								</div>

							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation3">
								<h3 class="mb-0 pb-0 text-uppercase">จัดการข้อมูลพนักงาน</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>ID</th>
													<th>ชื่อ</th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$employees=DB::table('employee')->get();
												foreach ($employees as $employee) {
													?>
													<tr>
														<td><?=$employee->id?></td>
														<td><?=$employee->name?></td>
														<td><a href="editeemployee/<?=$employee->id?>" title="">แก้ไข</a></td>
														<td><a href="deleteemployee/<?=$employee->id?>" title="">ลบ</a></td>
													</tr>
													<?php 
												}
												?>
											</tbody>
										</table>

									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<a href="/addemployee" title="">เพิ่มข้อมูลพนักงาน</a>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation4">
								<h3 class="mb-0 pb-0 text-uppercase">จัดการข้อมูลโปรโมชั่น</h3>
								<div class="divider divider-primary divider-small mb-4 mt-0">
									<hr class="mt-2 mr-auto">
								</div>

								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>ID</th>
													<th>ชื่อโปรโมชั่น</th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$promotions=DB::table('promotion')->get();
												foreach ($promotions as $promotion) {
													?>
													<tr>
														<td><?=$promotion->id?></td>
														<td><?=$promotion->name?></td>
														<td><a href="editepromotion/<?=$promotion->id?>" title="">แก้ไข</a></td>
														<td><a href="deletepromotion/<?=$promotion->id?>" title="">ลบ</a></td>
													</tr>
													<?php 
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<a href="/addpromotion" title="">เพิ่มข้อมูลโปรโมชั่น</a>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>
			<?=view('footer');?>
		</div>

	</div>
	<?=view('js');?>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function() {
		$('#about').addClass('active');
	});
</script>
